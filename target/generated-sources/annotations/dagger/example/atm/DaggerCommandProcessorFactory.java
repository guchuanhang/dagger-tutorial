package dagger.example.atm;

import com.google.common.collect.ImmutableMap;
import dagger.internal.DaggerGenerated;
import dagger.internal.DoubleCheck;
import dagger.internal.InstanceFactory;
import dagger.internal.MapFactory;
import dagger.internal.Preconditions;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
final class DaggerCommandProcessorFactory implements CommandProcessorFactory {
  /**
   * A {@link Provider} that returns {@code Optional.empty()}.
   */
  @SuppressWarnings("rawtypes")
  private static final Provider ABSENT_JDK_OPTIONAL_PROVIDER = InstanceFactory.create(Optional.empty());

  private final DaggerCommandProcessorFactory commandProcessorFactory = this;

  private Provider<HelloWorldCommand> helloWorldCommandProvider;

  private Provider<Optional<Database.Account>> optionalOfAccountProvider;

  private Provider<UserCommandsRouter.Factory> userCommandsRouterFactoryProvider;

  private Provider<LoginCommand> loginCommandProvider;

  private Provider<Map<String, Command>> mapOfStringAndCommandProvider;

  private Provider<CommandRouter> commandRouterProvider;

  private Provider<CommandProcessor> commandProcessorProvider;

  private Provider<InMemoryDatabase> inMemoryDatabaseProvider;

  private DaggerCommandProcessorFactory() {

    initialize();

  }

  public static Builder builder() {
    return new Builder();
  }

  public static CommandProcessorFactory create() {
    return new Builder().build();
  }

  private HelloWorldCommand helloWorldCommand() {
    return new HelloWorldCommand(SystemOutModule_TextOutputterFactory.textOutputter());
  }

  @SuppressWarnings("unchecked")
  private void initialize() {
    this.helloWorldCommandProvider = HelloWorldCommand_Factory.create(SystemOutModule_TextOutputterFactory.create());
    this.optionalOfAccountProvider = absentJdkOptionalProvider();
    this.userCommandsRouterFactoryProvider = new Provider<UserCommandsRouter.Factory>() {
      @Override
      public UserCommandsRouter.Factory get() {
        return new UserCommandsRouterFactory(commandProcessorFactory);
      }
    };
    this.loginCommandProvider = LoginCommand_Factory.create(SystemOutModule_TextOutputterFactory.create(), optionalOfAccountProvider, userCommandsRouterFactoryProvider);
    this.mapOfStringAndCommandProvider = MapFactory.<String, Command>builder(2).put("hello", ((Provider) helloWorldCommandProvider)).put("login", ((Provider) loginCommandProvider)).build();
    this.commandRouterProvider = CommandRouter_Factory.create(mapOfStringAndCommandProvider, SystemOutModule_TextOutputterFactory.create());
    this.commandProcessorProvider = DoubleCheck.provider(CommandProcessor_Factory.create(commandRouterProvider));
    this.inMemoryDatabaseProvider = DoubleCheck.provider(InMemoryDatabase_Factory.create());
  }

  @Override
  public CommandProcessor commandProcessor() {
    return commandProcessorProvider.get();
  }

  /**
   * Returns a {@link Provider} that returns {@code Optional.empty()}.
   */
  private static <T> Provider<Optional<T>> absentJdkOptionalProvider() {
    @SuppressWarnings("unchecked") // safe covariant cast
    Provider<Optional<T>> provider = (Provider<Optional<T>>) ABSENT_JDK_OPTIONAL_PROVIDER;
    return provider;
  }

  static final class Builder {
    private Builder() {
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder systemOutModule(SystemOutModule systemOutModule) {
      Preconditions.checkNotNull(systemOutModule);
      return this;
    }

    public CommandProcessorFactory build() {
      return new DaggerCommandProcessorFactory();
    }
  }

  private static final class UserCommandsRouterFactory implements UserCommandsRouter.Factory {
    private final DaggerCommandProcessorFactory commandProcessorFactory;

    private UserCommandsRouterFactory(DaggerCommandProcessorFactory commandProcessorFactory) {
      this.commandProcessorFactory = commandProcessorFactory;
    }

    @Override
    public UserCommandsRouter create(String username) {
      Preconditions.checkNotNull(username);
      return new UserCommandsRouterImpl(commandProcessorFactory, username);
    }
  }

  private static final class UserCommandsRouterImpl implements UserCommandsRouter {
    private final String username;

    private final DaggerCommandProcessorFactory commandProcessorFactory;

    private final UserCommandsRouterImpl userCommandsRouterImpl = this;

    private Provider<WithdrawalLimiter> withdrawalLimiterProvider;

    private UserCommandsRouterImpl(DaggerCommandProcessorFactory commandProcessorFactory,
        String usernameParam) {
      this.commandProcessorFactory = commandProcessorFactory;
      this.username = usernameParam;
      initialize(usernameParam);

    }

    private Database.Account account() {
      return AccountModule_AccountFactory.account(commandProcessorFactory.inMemoryDatabaseProvider.get(), username);
    }

    private LoginCommand loginCommand() {
      return new LoginCommand(SystemOutModule_TextOutputterFactory.textOutputter(), Optional.of(account()), new UserCommandsRouterFactory(commandProcessorFactory));
    }

    private DepositCommand depositCommand() {
      return new DepositCommand(SystemOutModule_TextOutputterFactory.textOutputter(), account(), withdrawalLimiterProvider.get());
    }

    private WithdrawCommand withdrawCommand() {
      return new WithdrawCommand(SystemOutModule_TextOutputterFactory.textOutputter(), account(), AmountsModule_MinimumBalanceFactory.minimumBalance(), withdrawalLimiterProvider.get());
    }

    private LogoutCommand logoutCommand() {
      return new LogoutCommand(SystemOutModule_TextOutputterFactory.textOutputter(), account());
    }

    private Map<String, Command> mapOfStringAndCommand() {
      return ImmutableMap.<String, Command>of("hello", commandProcessorFactory.helloWorldCommand(), "login", loginCommand(), "deposit", depositCommand(), "withdraw", withdrawCommand(), "logout", logoutCommand());
    }

    @SuppressWarnings("unchecked")
    private void initialize(final String usernameParam) {
      this.withdrawalLimiterProvider = DoubleCheck.provider(WithdrawalLimiter_Factory.create(AmountsModule_MaximumWithdrawalFactory.create()));
    }

    @Override
    public CommandRouter router() {
      return new CommandRouter(mapOfStringAndCommand(), SystemOutModule_TextOutputterFactory.textOutputter());
    }
  }
}
