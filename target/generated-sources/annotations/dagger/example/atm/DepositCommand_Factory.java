package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DepositCommand_Factory implements Factory<DepositCommand> {
  private final Provider<Outputter> outputterProvider;

  private final Provider<Database.Account> accountProvider;

  private final Provider<WithdrawalLimiter> withdrawalLimiterProvider;

  public DepositCommand_Factory(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider,
      Provider<WithdrawalLimiter> withdrawalLimiterProvider) {
    this.outputterProvider = outputterProvider;
    this.accountProvider = accountProvider;
    this.withdrawalLimiterProvider = withdrawalLimiterProvider;
  }

  @Override
  public DepositCommand get() {
    return newInstance(outputterProvider.get(), accountProvider.get(), withdrawalLimiterProvider.get());
  }

  public static DepositCommand_Factory create(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider,
      Provider<WithdrawalLimiter> withdrawalLimiterProvider) {
    return new DepositCommand_Factory(outputterProvider, accountProvider, withdrawalLimiterProvider);
  }

  public static DepositCommand newInstance(Object outputter, Object account,
      Object withdrawalLimiter) {
    return new DepositCommand((Outputter) outputter, (Database.Account) account, (WithdrawalLimiter) withdrawalLimiter);
  }
}
