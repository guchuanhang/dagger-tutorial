package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import javax.annotation.Generated;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SystemOutModule_Proxy {
  private SystemOutModule_Proxy() {
  }

  public static SystemOutModule newInstance() {
    return new SystemOutModule();
  }
}
