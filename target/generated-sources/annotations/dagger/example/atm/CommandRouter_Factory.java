package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.util.Map;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CommandRouter_Factory implements Factory<CommandRouter> {
  private final Provider<Map<String, Command>> commandsProvider;

  private final Provider<Outputter> outputterProvider;

  public CommandRouter_Factory(Provider<Map<String, Command>> commandsProvider,
      Provider<Outputter> outputterProvider) {
    this.commandsProvider = commandsProvider;
    this.outputterProvider = outputterProvider;
  }

  @Override
  public CommandRouter get() {
    return newInstance(commandsProvider.get(), outputterProvider.get());
  }

  public static CommandRouter_Factory create(Provider<Map<String, Command>> commandsProvider,
      Provider<Outputter> outputterProvider) {
    return new CommandRouter_Factory(commandsProvider, outputterProvider);
  }

  public static CommandRouter newInstance(Map<String, Command> commands, Object outputter) {
    return new CommandRouter(commands, (Outputter) outputter);
  }
}
