package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.math.BigDecimal;
import javax.annotation.Generated;

@ScopeMetadata
@QualifierMetadata("dagger.example.atm.MinimumBalance")
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AmountsModule_MinimumBalanceFactory implements Factory<BigDecimal> {
  @Override
  public BigDecimal get() {
    return minimumBalance();
  }

  public static AmountsModule_MinimumBalanceFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static BigDecimal minimumBalance() {
    return Preconditions.checkNotNullFromProvides(AmountsModule.minimumBalance());
  }

  private static final class InstanceHolder {
    private static final AmountsModule_MinimumBalanceFactory INSTANCE = new AmountsModule_MinimumBalanceFactory();
  }
}
