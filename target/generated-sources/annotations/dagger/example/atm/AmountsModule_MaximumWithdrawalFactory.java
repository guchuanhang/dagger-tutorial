package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.math.BigDecimal;
import javax.annotation.Generated;

@ScopeMetadata
@QualifierMetadata("dagger.example.atm.MaximumWithdrawal")
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AmountsModule_MaximumWithdrawalFactory implements Factory<BigDecimal> {
  @Override
  public BigDecimal get() {
    return maximumWithdrawal();
  }

  public static AmountsModule_MaximumWithdrawalFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static BigDecimal maximumWithdrawal() {
    return Preconditions.checkNotNullFromProvides(AmountsModule.maximumWithdrawal());
  }

  private static final class InstanceHolder {
    private static final AmountsModule_MaximumWithdrawalFactory INSTANCE = new AmountsModule_MaximumWithdrawalFactory();
  }
}
