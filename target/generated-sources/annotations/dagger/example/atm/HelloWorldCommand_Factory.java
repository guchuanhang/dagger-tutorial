package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class HelloWorldCommand_Factory implements Factory<HelloWorldCommand> {
  private final Provider<Outputter> outputterProvider;

  public HelloWorldCommand_Factory(Provider<Outputter> outputterProvider) {
    this.outputterProvider = outputterProvider;
  }

  @Override
  public HelloWorldCommand get() {
    return newInstance(outputterProvider.get());
  }

  public static HelloWorldCommand_Factory create(Provider<Outputter> outputterProvider) {
    return new HelloWorldCommand_Factory(outputterProvider);
  }

  public static HelloWorldCommand newInstance(Object outputter) {
    return new HelloWorldCommand((Outputter) outputter);
  }
}
