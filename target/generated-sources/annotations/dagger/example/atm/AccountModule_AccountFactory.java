package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata("dagger.example.atm.Username")
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AccountModule_AccountFactory implements Factory<Database.Account> {
  private final Provider<Database> databaseProvider;

  private final Provider<String> usernameProvider;

  public AccountModule_AccountFactory(Provider<Database> databaseProvider,
      Provider<String> usernameProvider) {
    this.databaseProvider = databaseProvider;
    this.usernameProvider = usernameProvider;
  }

  @Override
  public Database.Account get() {
    return account(databaseProvider.get(), usernameProvider.get());
  }

  public static AccountModule_AccountFactory create(Provider<Database> databaseProvider,
      Provider<String> usernameProvider) {
    return new AccountModule_AccountFactory(databaseProvider, usernameProvider);
  }

  public static Database.Account account(Object database, String username) {
    return Preconditions.checkNotNullFromProvides(AccountModule.account((Database) database, username));
  }
}
