package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CommandProcessor_Factory implements Factory<CommandProcessor> {
  private final Provider<CommandRouter> firstCommandRouterProvider;

  public CommandProcessor_Factory(Provider<CommandRouter> firstCommandRouterProvider) {
    this.firstCommandRouterProvider = firstCommandRouterProvider;
  }

  @Override
  public CommandProcessor get() {
    return newInstance(firstCommandRouterProvider.get());
  }

  public static CommandProcessor_Factory create(
      Provider<CommandRouter> firstCommandRouterProvider) {
    return new CommandProcessor_Factory(firstCommandRouterProvider);
  }

  public static CommandProcessor newInstance(Object firstCommandRouter) {
    return new CommandProcessor((CommandRouter) firstCommandRouter);
  }
}
