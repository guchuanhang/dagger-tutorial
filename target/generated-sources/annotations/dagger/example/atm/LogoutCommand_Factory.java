package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LogoutCommand_Factory implements Factory<LogoutCommand> {
  private final Provider<Outputter> outputterProvider;

  private final Provider<Database.Account> accountProvider;

  public LogoutCommand_Factory(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider) {
    this.outputterProvider = outputterProvider;
    this.accountProvider = accountProvider;
  }

  @Override
  public LogoutCommand get() {
    return newInstance(outputterProvider.get(), accountProvider.get());
  }

  public static LogoutCommand_Factory create(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider) {
    return new LogoutCommand_Factory(outputterProvider, accountProvider);
  }

  public static LogoutCommand newInstance(Object outputter, Object account) {
    return new LogoutCommand((Outputter) outputter, (Database.Account) account);
  }
}
