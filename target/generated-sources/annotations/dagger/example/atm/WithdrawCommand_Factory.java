package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata("dagger.example.atm.MinimumBalance")
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class WithdrawCommand_Factory implements Factory<WithdrawCommand> {
  private final Provider<Outputter> outputterProvider;

  private final Provider<Database.Account> accountProvider;

  private final Provider<BigDecimal> minimumBalanceProvider;

  private final Provider<WithdrawalLimiter> withdrawalLimiterProvider;

  public WithdrawCommand_Factory(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider, Provider<BigDecimal> minimumBalanceProvider,
      Provider<WithdrawalLimiter> withdrawalLimiterProvider) {
    this.outputterProvider = outputterProvider;
    this.accountProvider = accountProvider;
    this.minimumBalanceProvider = minimumBalanceProvider;
    this.withdrawalLimiterProvider = withdrawalLimiterProvider;
  }

  @Override
  public WithdrawCommand get() {
    return newInstance(outputterProvider.get(), accountProvider.get(), minimumBalanceProvider.get(), withdrawalLimiterProvider.get());
  }

  public static WithdrawCommand_Factory create(Provider<Outputter> outputterProvider,
      Provider<Database.Account> accountProvider, Provider<BigDecimal> minimumBalanceProvider,
      Provider<WithdrawalLimiter> withdrawalLimiterProvider) {
    return new WithdrawCommand_Factory(outputterProvider, accountProvider, minimumBalanceProvider, withdrawalLimiterProvider);
  }

  public static WithdrawCommand newInstance(Object outputter, Object account,
      BigDecimal minimumBalance, Object withdrawalLimiter) {
    return new WithdrawCommand((Outputter) outputter, (Database.Account) account, minimumBalance, (WithdrawalLimiter) withdrawalLimiter);
  }
}
