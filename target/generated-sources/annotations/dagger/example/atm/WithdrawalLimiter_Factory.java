package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata("dagger.example.atm.PerSession")
@QualifierMetadata("dagger.example.atm.MaximumWithdrawal")
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class WithdrawalLimiter_Factory implements Factory<WithdrawalLimiter> {
  private final Provider<BigDecimal> maximumWithdrawalProvider;

  public WithdrawalLimiter_Factory(Provider<BigDecimal> maximumWithdrawalProvider) {
    this.maximumWithdrawalProvider = maximumWithdrawalProvider;
  }

  @Override
  public WithdrawalLimiter get() {
    return newInstance(maximumWithdrawalProvider.get());
  }

  public static WithdrawalLimiter_Factory create(Provider<BigDecimal> maximumWithdrawalProvider) {
    return new WithdrawalLimiter_Factory(maximumWithdrawalProvider);
  }

  public static WithdrawalLimiter newInstance(BigDecimal maximumWithdrawal) {
    return new WithdrawalLimiter(maximumWithdrawal);
  }
}
