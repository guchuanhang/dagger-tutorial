package dagger.example.atm;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import java.util.Optional;
import javax.annotation.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoginCommand_Factory implements Factory<LoginCommand> {
  private final Provider<Outputter> outputterProvider;

  private final Provider<Optional<Database.Account>> accountProvider;

  private final Provider<UserCommandsRouter.Factory> userCommandsFactoryProvider;

  public LoginCommand_Factory(Provider<Outputter> outputterProvider,
      Provider<Optional<Database.Account>> accountProvider,
      Provider<UserCommandsRouter.Factory> userCommandsFactoryProvider) {
    this.outputterProvider = outputterProvider;
    this.accountProvider = accountProvider;
    this.userCommandsFactoryProvider = userCommandsFactoryProvider;
  }

  @Override
  public LoginCommand get() {
    return newInstance(outputterProvider.get(), accountProvider.get(), userCommandsFactoryProvider.get());
  }

  public static LoginCommand_Factory create(Provider<Outputter> outputterProvider,
      Provider<Optional<Database.Account>> accountProvider,
      Provider<UserCommandsRouter.Factory> userCommandsFactoryProvider) {
    return new LoginCommand_Factory(outputterProvider, accountProvider, userCommandsFactoryProvider);
  }

  public static LoginCommand newInstance(Object outputter, Optional<Database.Account> account,
      Object userCommandsFactory) {
    return new LoginCommand((Outputter) outputter, account, (UserCommandsRouter.Factory) userCommandsFactory);
  }
}
